import { ConfigObject } from '../api/model/index';
export declare function create(sessionId?: string, config?: ConfigObject, customUserAgent?: string): any;
