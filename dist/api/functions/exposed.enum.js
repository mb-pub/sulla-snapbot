"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ExposedFn;
(function (ExposedFn) {
    ExposedFn["OnMessage"] = "onMessage";
    ExposedFn["OnAnyMessage"] = "onAnyMessage";
    ExposedFn["onAck"] = "onAck";
    ExposedFn["onParticipantsChanged"] = "onParticipantsChanged";
    ExposedFn["onStateChanged"] = "onStateChanged";
})(ExposedFn = exports.ExposedFn || (exports.ExposedFn = {}));
